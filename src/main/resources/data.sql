INSERT INTO Student (id, name, surname, email, password, phone) VALUES
(1, 'Kraemer', 'Joe', 'booly57@gmail.com', 'hardpassword', '06333666699'),
(2, 'Ragnar', 'Hugo', 'dsqdqs@gmail.com', 'hardpzaeazezaassword', '556456465454'),
(3, 'Lodbrok', 'Martin', 'gsdgfsdg@gmail.com', 'hardprgfdvcwassword', '7987986456'),
(4, 'Lagerta', 'Tod', 'boozaeazeazely57@gmail.com', 'dqsdqsdqsd', '002345664654');

INSERT INTO Classroom (id, name) VALUES
(1, 123),
(2, 221),
(3, 109),
(4, 666);

INSERT INTO Course (id, name) VALUES
(1, 'Maths'),
(2, 'History'),
(3, 'RodislavTheGOATCourse');

INSERT INTO Teacher (id, name, surname, email, password, phone) VALUES
(1, 'LeSang', 'TeacherOne', 'teacherOne@gmail.com', 'hardpassword', '06333666699'),
(2, 'JCVD', 'TeacherTwo', 'teacherTwo@gmail.com', 'hardpzaeazezaassword', '556456465454');

INSERT INTO Schedule (id, classroom_id, course_id, datetime) VALUES
(1, 1, 1, '15/03/2022 15:00:00'),
(2, 2, 1, '16/03/2022 16:00:00');