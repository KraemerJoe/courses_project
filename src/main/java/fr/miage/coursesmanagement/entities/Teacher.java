package fr.miage.coursesmanagement.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor

public class Teacher{

    @Id
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
}
