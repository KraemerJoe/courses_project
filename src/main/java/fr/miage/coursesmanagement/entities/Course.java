package fr.miage.coursesmanagement.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Course {

    @Id
    private Long id;
    private String name;

;

}
