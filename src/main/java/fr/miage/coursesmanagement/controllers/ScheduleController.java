package fr.miage.coursesmanagement.controllers;

import fr.miage.coursesmanagement.entities.Classroom;
import fr.miage.coursesmanagement.entities.Course;
import fr.miage.coursesmanagement.entities.Schedule;
import fr.miage.coursesmanagement.repositories.ClassroomRepository;
import fr.miage.coursesmanagement.repositories.ScheduleRepository;
import fr.miage.coursesmanagement.repositories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/schedules")
public class ScheduleController {

    private final TeacherRepository tr;
    private final ScheduleRepository sr;
    private final ClassroomRepository crr;

    @GetMapping
    public ResponseEntity<?> getAllSchedules(){
        return ResponseEntity.ok(sr.findAll());
    }

    @PostMapping("/{scheduleId}/{roomId}")
    public ResponseEntity<?> editScheduleRoom(@PathVariable("scheduleId") Long scheduleId, @PathVariable("roomId") Long roomId){
        Schedule schedule = sr.getById(scheduleId);
        Classroom classroom = crr.getById(roomId);
        schedule.setClassroom(classroom);
        sr.save(schedule);

        return ResponseEntity.ok().build();
    }

}
