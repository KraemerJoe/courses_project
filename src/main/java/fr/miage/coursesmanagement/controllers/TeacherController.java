package fr.miage.coursesmanagement.controllers;

import fr.miage.coursesmanagement.repositories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/teachers")
public class TeacherController {

    private final TeacherRepository tr;

    @GetMapping
    public ResponseEntity<?> getAllTeachers(){
        return ResponseEntity.ok(tr.findAll());
    }
}
