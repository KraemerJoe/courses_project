package fr.miage.coursesmanagement.repositories;

import fr.miage.coursesmanagement.entities.Classroom;
import fr.miage.coursesmanagement.entities.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
}
