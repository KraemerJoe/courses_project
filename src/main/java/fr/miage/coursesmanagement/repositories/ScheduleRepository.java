package fr.miage.coursesmanagement.repositories;

import fr.miage.coursesmanagement.entities.Course;
import fr.miage.coursesmanagement.entities.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
}
